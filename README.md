# Start by looking at the slacklexopenfaas.py main.  the docker-compose is used for another use of the bot #

# README #

Initial proof of concept for mapping Lex to Openfaas.

### What is this repository for? ###

Provids Slack bot service which when bot is mentioned forwards on text to Lex.  
It then pasess the Lex requests for more info back to the slack channel so the user can respond.

When Lex has decided it has enough information to fufill an intent, this service maps the intent to a function in openfaas.

What are the benefits?  You could add new Intents over in the Lex gui, 
and then go directly over to your openfaas and create a function which
maps by convention to the name of intent.  No services need to get restarted.  Just add/remove new functionality
and it should work.

Currently you need to change your function yml file to create a lowercase docker image as caps are not permitted.  
Might make more sense to change intent to lower case so function name matches the image name.

### How do I get set up? ###

There is still some hardcoding around connecting to Lex, but that can fixed quite easily.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

If you have any feedback, questions or ideas pass on to craigh at quailholdings.com, or look for Craig Hamilton on the openfass Slack workgroup.

