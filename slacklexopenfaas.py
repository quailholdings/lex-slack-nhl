import logging
import requests
import json
import os
from slacklex import Lex,SlackBot

try:
    import graypy

    gray_log_avail = True
except ImportError:
    gray_log_avail = False

log = logging.getLogger()

debug_ide = os.getenv('debug_ide') or False
# if graypy avail, let's use it.  unless in debug in ide mode
if gray_log_avail and not debug_ide:
    # let's log to gelf
    gelf_url = '192.168.1.25'
    handler = graypy.GELFHandler(gelf_url, 12201, localname='nhl-lex-slack-openfaas')
    log.addHandler(handler)

log.setLevel(logging.DEBUG)


# load swarm secrets
def get_secret(secret_name):
    try:
        with open('/run/secrets/{0}'.format(secret_name), 'r') as secret_file:
            return secret_file.read().rstrip()
    except IOError:
        return None


slack_access_key_id = get_secret('slack_nhl_bot') or os.getenv('slack_nhl_bot')
slack_lex_openfaas_id = get_secret('slack_nhl_bot_id') or os.getenv('slack_nhl_bot_id')
open_faas_url = get_secret('open_faas_url') or os.getenv('open_faas_url')

access_key_id = get_secret('aws_access_key_id') or os.getenv('aws_access_key_id')
secret_access_key = get_secret('aws_secret_access_key') or os.getenv('aws_secret_access_key')


if __name__ == "__main__":
    lex_client = Lex()
    bot = SlackBot(slack_access_key_id, slack_lex_openfaas_id)

    def my_callback(command,channel):
        log.debug("Sending to lex: {}".format(command))
        lex_response = lex_client.send(command)
        log.debug("response from lex: {}".format(lex_response))
        if lex_response['dialogState'] == 'ReadyForFulfillment':
            url = open_faas_url
            # call openfaas, by convention function name is intent
            call = {'intentName':lex_response['intentName'], 'slots':lex_response['slots']}
            faas_response = requests.post(url + lex_response['intentName'], json=json.dumps(call))
            log.debug("Response from openfaas app: {}".format(faas_response.text))
            bot.send_message(faas_response.text, channel)
        else:
            bot.send_message(lex_response['message'], channel)

    bot.on_message_received(my_callback)

    try:
        bot.start_listening()
    except Exception as e:
        log.exception(e)
