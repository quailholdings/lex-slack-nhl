import logging
import time
import boto3
import requests
import json
import os
from slackclient import SlackClient

try:
    import graypy
    gray_log_avail = True
except ImportError:
    gray_log_avail = False

log = logging.getLogger()

debug_ide = os.getenv('debug_ide') or False


# if graypy avail, let's use it.  unless in debug in ide mode
if gray_log_avail and not debug_ide:
    # let's log to gelf
    gelf_url = '192.168.1.20'
    handler = graypy.GELFHandler(gelf_url, 12201, localname='nhl-lex-slack')
    log.addHandler(handler)

log.setLevel(logging.DEBUG)


# load swarm secrets
def get_secret(secret_name):
    try:
        with open('/run/secrets/{0}'.format(secret_name), 'r') as secret_file:
            return secret_file.read().rstrip()
    except IOError:
        return None


slack_access_key_id = get_secret('slack_nhl_bot') or os.getenv('slack_nhl_bot')


access_key_id = get_secret('aws_access_key_id') or os.getenv('aws_access_key_id')
secret_access_key = get_secret('aws_secret_access_key') or os.getenv('aws_secret_access_key')

class SlackBot:
    log = logging.getLogger(__name__)

    def __init__(self, access_key_token, bot_id):
        self.slack_client = SlackClient(access_key_token)
        self.bot_id = "<@" + bot_id + ">"

    def send_message(self,message, channel):
        self.slack_client.rtm_send_message(channel,message)

    def on_message_received(self,callback):
        self._on_message_received = callback

    def parse_slack_output(self, slack_rtm_output):
        """
            The Slack Real Time Messaging API is an events firehose.
            this parsing function returns None unless a message is
            directed at the Bot, based on its ID.
        """
        output_list = slack_rtm_output
        if output_list and len(output_list) > 0:
            for output in output_list:
                if output and 'text' in output and self.bot_id  in output['text']:
                    # return text after the @ mention, whitespace removed
                    return output['text'].split(self.bot_id )[1].strip().lower(), \
                           output['channel'], output['user']
        return None, None, None

    def start_listening(self):
        READ_WEBSOCKET_DELAY = 1  # 1 second delay between reading from firehose
        if self.slack_client.rtm_connect():
            log.debug("StarterBot connected and running!")
            while True:
                command, channel, user = self.parse_slack_output(self.slack_client.rtm_read())
                if command and channel and user:
                    # handle_command(command, channel)
                    if self._on_message_received: self._on_message_received(command,channel,user)
                time.sleep(READ_WEBSOCKET_DELAY)
        else:
            log.debug("Connection failed. Invalid Slack token or bot ID?")


class Lex:
    def __init__(self,bot_name,bot_alias):
        self.bot_name = bot_name
        self.bot_alias = bot_alias
        self.client = boto3.client('lex-runtime', aws_access_key_id=access_key_id,aws_secret_access_key=secret_access_key, region_name='us-east-1')

    def send(self,text,user):
        response = self.client.post_text(
            botName=self.bot_name,
            botAlias=self.bot_alias,
            userId=user,
            sessionAttributes={
                'string': 'string'
            },
            requestAttributes={
                'string': 'string'
            },
            inputText=text
        )
        return response


if __name__ == "__main__":
    slack_lex_bot_id = get_secret('slack_nhl_bot_id') or os.getenv('slack_nhl_bot_id')
    nhl_service_url = os.getenv("nhl_service_url")
    lex_client = Lex(bot_name='test',bot_alias='slack')
    m = SlackBot(slack_access_key_id, slack_lex_bot_id)

    def my_callback(command,channel,user):
        log.debug("Sending to lex: {}".format(command))
        response = lex_client.send(command,user)
        log.debug("response from lex: {}".format(response))
        if response['dialogState'] == 'ReadyForFulfillment':
            url = nhl_service_url
            # call nhl app
            call = {'intentName':response['intentName'], 'slots':response['slots']}
            log.debug("calling nhl app: {}".format(nhl_service_url))
            r = requests.post(url, json=json.dumps(call))
            log.debug("Response from nhl app: {}".format(r.text))
            m.send_message(r.text,channel)
        else:
            m.send_message(response['message'],channel)

    m.on_message_received(my_callback)

    try:
        m.start_listening()
    except Exception as e:
        log.exception(e)
